package com.RommelPalma.conversordegrados;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText valor1;
    TextView vista1;
    Button fahrenheit, celcius;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        valor1 = findViewById(R.id.valor1);
        vista1 = findViewById(R.id.vista1);
        fahrenheit = findViewById(R.id.fahrenheit);
        celcius = findViewById(R.id.celcius);

        fahrenheit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String entradafahrenheit = valor1.getText().toString();
                double entradafahrenheitint = Integer.parseInt(entradafahrenheit);

                double fahrenheit = (entradafahrenheitint * 1.8) + 32;
                vista1.setText(fahrenheit + "ºF");
            }
        });

        celcius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String entradacelcius = valor1.getText().toString();
                double entradacelciusint = Integer.parseInt(entradacelcius);

                double celcius = (entradacelciusint - 32 ) * 5/9;
                vista1.setText(celcius + "ºC");
            }
        });
    }
}